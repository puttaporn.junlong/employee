package com.puttaporn.employee.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.puttaporn.employee.common.controller.AbsRestController;
import com.puttaporn.employee.common.model.AbsResponseModel;
import com.puttaporn.employee.exceptions.BusinessException;
import com.puttaporn.employee.model.request.GetInfoRequest;
import com.puttaporn.employee.model.request.RegisterEmployeeRequest;
import com.puttaporn.employee.model.response.GetInfoResponse;
import com.puttaporn.employee.model.response.RegisterEmployeeResponse;
import com.puttaporn.employee.service.EmployeeService;

@RestController
@RequestMapping(value = "/api/employee")
public class EmployeeController extends AbsRestController {
	
	@Autowired
	private EmployeeService employeeService;
	
	@PostMapping(value = "/register", produces= {MediaType.APPLICATION_JSON_VALUE}, consumes= {MediaType.APPLICATION_JSON_VALUE})
	public @ResponseBody AbsResponseModel RegisterEmployee(HttpServletRequest request, @RequestBody RegisterEmployeeRequest requestMsgModel, HttpServletResponse response) throws Exception {
		try {
			RegisterEmployeeResponse registerEmployeeResponse = employeeService.registerEmployee(requestMsgModel);
			return registerEmployeeResponse;
		}catch(BusinessException bex) {
			return errorResponse(bex);
		}catch(Exception ex) {
			ex.printStackTrace();
			return errorResponse(ex);
		}
		
	}
	
	@PostMapping(value = "/getinfo", produces= {MediaType.APPLICATION_JSON_VALUE}, consumes= {MediaType.APPLICATION_JSON_VALUE})
	public @ResponseBody AbsResponseModel GetInfo(HttpServletRequest request, @RequestBody GetInfoRequest requestMsgModel, HttpServletResponse response) throws Exception {
		try {
			GetInfoResponse getInfoResponse = employeeService.getInfo(requestMsgModel.getToken());
			return getInfoResponse;
		}catch(Exception ex) {
			return errorResponse(ex);
		}
		
	}
}
