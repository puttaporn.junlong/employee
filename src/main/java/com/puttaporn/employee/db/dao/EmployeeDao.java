package com.puttaporn.employee.db.dao;

import com.puttaporn.employee.model.data.EmployeeModel;

public interface EmployeeDao {
	public int insert(EmployeeModel emp);
	public EmployeeModel selectById(String employeeId);
	
}
