package com.puttaporn.employee.db.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.puttaporn.employee.common.db.dao.AbsDao;
import com.puttaporn.employee.db.dao.EmployeeDao;
import com.puttaporn.employee.model.data.EmployeeModel;

@Repository
public class EmployeeDaoImpl extends AbsDao implements EmployeeDao{

	@Override
	public int insert(EmployeeModel emp) {
		
		String sql = "INSERT INTO employee (employee_id, username, password, address, phone, type, firstname, lastname, salary) VALUES (:id, :username, :password, :address, :phone, :type, :firstname"
				+ ", :lastname, :salary)";
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
	    paramMap.put("id", emp.getEmployeeId());
	    paramMap.put("username", emp.getUsername());
	    paramMap.put("password", emp.getPassword());
	    paramMap.put("address", emp.getAddress());
	    paramMap.put("phone", emp.getPhone());
	    paramMap.put("type", emp.getType());
	    paramMap.put("firstname", emp.getFirstname());
	    paramMap.put("lastname", emp.getLastname());
	    paramMap.put("salary", emp.getSalary());
	    return namedParameterJdbcTemplate.update(sql, paramMap);  
	}

	@Override
	public EmployeeModel selectById(String employeeId) {
		String sql = "SELECT employee_id, username, password, address, phone, type, firstname, lastname, salary FROM employee WHERE employee_id = :id";
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
	    paramMap.put("id", employeeId);
	    
	    List<EmployeeModel> list = namedParameterJdbcTemplate.query(sql, paramMap,
				new BeanPropertyRowMapper<EmployeeModel>(EmployeeModel.class));
		return (list!=null&&!list.isEmpty())?list.get(0):null;
	}
	
	
	
	

}
