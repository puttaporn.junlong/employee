package com.puttaporn.employee.model.request;

import com.puttaporn.employee.common.model.AbsRequestModel;

public class GetInfoRequest extends AbsRequestModel{

	private String token;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
	
	
	
}
