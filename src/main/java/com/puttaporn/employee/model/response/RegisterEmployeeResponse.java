package com.puttaporn.employee.model.response;

import com.puttaporn.employee.common.model.AbsResponseModel;

public class RegisterEmployeeResponse  extends AbsResponseModel {
	
	private String token;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
	
	

}
