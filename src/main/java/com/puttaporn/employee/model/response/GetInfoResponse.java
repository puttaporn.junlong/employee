package com.puttaporn.employee.model.response;

import com.puttaporn.employee.common.model.AbsResponseModel;
import com.puttaporn.employee.model.data.EmployeeModel;

public class GetInfoResponse extends AbsResponseModel{

	private EmployeeModel employee;

	public EmployeeModel getEmployee() {
		return employee;
	}

	public void setEmployee(EmployeeModel employee) {
		this.employee = employee;
	}
	
}
