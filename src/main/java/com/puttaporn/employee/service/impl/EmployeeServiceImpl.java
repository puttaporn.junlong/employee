package com.puttaporn.employee.service.impl;

import java.math.BigDecimal;
import java.security.Key;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.puttaporn.employee.db.dao.EmployeeDao;
import com.puttaporn.employee.exceptions.BusinessException;
import com.puttaporn.employee.model.data.EmployeeModel;
import com.puttaporn.employee.model.request.RegisterEmployeeRequest;
import com.puttaporn.employee.model.response.GetInfoResponse;
import com.puttaporn.employee.model.response.RegisterEmployeeResponse;
import com.puttaporn.employee.service.EmployeeService;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;


@Component
public class EmployeeServiceImpl implements EmployeeService{
	
	static final long EXPIRATION_TIME = 1000 * 60 * 5;
	static final String SECRET_KEY = "g5OKtFqgZuJYCg6oicA73vU3V0RFiKTuD6z3gcrvxgZk9lltykUIbT2i5l1sRsKufAU2HjU5LcGE99dGvwqpzaN4Lb6gXGBFI1w";
	
	@Autowired
	private EmployeeDao employeeDao;
	
	@Override
	public RegisterEmployeeResponse registerEmployee(RegisterEmployeeRequest empReq) throws Exception {
		if(empReq.getUsername()==null||empReq.getUsername().equals("")) {
			throw new BusinessException("EMP000", "Invalid input username");
		}
		if(empReq.getPassword()==null||empReq.getPassword().equals("")) {
			throw new BusinessException("EMP000", "Invalid input password");
		}
		EmployeeModel emp = new EmployeeModel();
		emp.setEmployeeId(generateEmployeeId(empReq.getPhone()));
		emp.setType(getMemberType(empReq.getSalary()));
		emp.setAddress(empReq.getAddress());
		emp.setFirstname(empReq.getFirstname());
		emp.setLastname(empReq.getLastname());
		emp.setPassword(empReq.getPassword());
		emp.setPhone(empReq.getPhone());
		emp.setSalary(empReq.getSalary());
		emp.setUsername(empReq.getUsername());
		
		employeeDao.insert(emp);
		
		RegisterEmployeeResponse response = new RegisterEmployeeResponse();
		response.setToken(createJWT(emp.getEmployeeId(), EXPIRATION_TIME));
		return response;
	}
	
	private String getMemberType(BigDecimal salary) throws Exception{
		
		if(salary==null) {
			throw new BusinessException("EMP000", "Invalid input salary");
		}
		
		if(salary.compareTo(new BigDecimal(15000))==-1) {
			throw new BusinessException("EMP002", "Salary less than 15000");
		}
		
		if(salary.compareTo(new BigDecimal(50000)) == 1) {
			return "Platinum";
		}else if(salary.compareTo(new BigDecimal(30000)) == 1 && salary.compareTo(new BigDecimal(50000)) == -1) {
			return "Gold";
		}else if(salary.compareTo(new BigDecimal(30000)) == 0) {
			return "Gold";
		}else if(salary.compareTo(new BigDecimal(50000)) == 0) {
			return "Gold";
		}else if(salary.compareTo(new BigDecimal(30000)) == -1) {
			return "Silver";
		}else {
			return "";
		}
	}
	
	private String generateEmployeeId(String phone) throws Exception{
		if(phone==null||phone.equals("")) {
			throw new BusinessException("EMP000", "Invalid input phone");
		}
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMdd");
		LocalDateTime now = LocalDateTime.now();
		
		String date = dtf.format(now);
		if (!phone.matches("^[0-9]*$")) {
			throw new BusinessException("EMP003", "Phone must contain number only");
		}
		
		String last4PhoneDigit = phone.substring(phone.length()-4);
		
		return date+last4PhoneDigit;
	}
	
	private String createJWT(String subject, long ttlMillis) {
		  
	    SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

	    long nowMillis = System.currentTimeMillis();
	    Date now = new Date(nowMillis);

	    byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(SECRET_KEY);
	    Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());

	    JwtBuilder builder = Jwts.builder()
	            .setIssuedAt(now)
	            .setSubject(subject)
	            .setExpiration(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
	            .signWith(signatureAlgorithm, signingKey);
	  
	    if (ttlMillis > 0) {
	        long expMillis = nowMillis + ttlMillis;
	        Date exp = new Date(expMillis);
	        builder.setExpiration(exp);
	    }  
	  
	    return builder.compact();
	}
	
	private Claims decodeJWT(String jwt) {
	    Claims claims = Jwts.parser()
	            .setSigningKey(DatatypeConverter.parseBase64Binary(SECRET_KEY))
	            .parseClaimsJws(jwt).getBody();
	    return claims;
	}

	@Override
	public GetInfoResponse getInfo(String token) {
		
		String employeeId = decodeJWT(token).getSubject();
		EmployeeModel employee = employeeDao.selectById(employeeId);
		
		GetInfoResponse GetInfoResponse = new GetInfoResponse();
		GetInfoResponse.setEmployee(employee);
		return GetInfoResponse;
	}
	
	

}
