package com.puttaporn.employee.service;

import com.puttaporn.employee.model.request.RegisterEmployeeRequest;
import com.puttaporn.employee.model.response.GetInfoResponse;
import com.puttaporn.employee.model.response.RegisterEmployeeResponse;

public interface EmployeeService {
	
	public RegisterEmployeeResponse registerEmployee(RegisterEmployeeRequest empReq)throws Exception;
	public GetInfoResponse getInfo(String token);

}
