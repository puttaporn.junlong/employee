package com.puttaporn.employee.exceptions;

public class BusinessException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String statusCode;
	private String message;
	
	public BusinessException(String statusCode, String message) {
		this.statusCode = statusCode;
		this.message = message;
	}
	
	public BusinessException(Throwable e) {
		this("EMP001", e.getMessage());
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}
