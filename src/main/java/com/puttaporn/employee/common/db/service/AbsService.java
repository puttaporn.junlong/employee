package com.puttaporn.employee.common.db.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

public abstract class AbsService {
	@Autowired
	@Qualifier("dataSourceTransactionManager")
	protected DataSourceTransactionManager transactionManager;
}
