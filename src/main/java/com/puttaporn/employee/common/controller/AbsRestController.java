package com.puttaporn.employee.common.controller;

import com.puttaporn.employee.common.model.CommonResponseModel;
import com.puttaporn.employee.exceptions.BusinessException;

public class AbsRestController {
	protected CommonResponseModel successResponse() {
		return new CommonResponseModel();
	}
	
	protected CommonResponseModel errorResponse(BusinessException bex) {
		return new CommonResponseModel(bex.getStatusCode(), bex.getMessage());
	}
	
	protected CommonResponseModel errorResponse(Exception ex) {
		BusinessException bex = new BusinessException(ex);
		return new CommonResponseModel(bex.getStatusCode(), bex.getMessage());
	}
}
