package com.puttaporn.employee.common.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ApplicationConfig {
	@Value("${application.version}")
	private String applicationVersion;
	
	//DB
	@Value("${spring.datasource.driver-class-name}")
	private String datasourceDriverClassName;
	@Value("${spring.datasource.url}")
	private String datasourceUrl;
	@Value("${spring.datasource.username}")
	private String datasourceUsername;
	@Value("${spring.datasource.password}")
	private String datasourcePassword;
	
	public String getApplicationVersion() {
		return applicationVersion;
	}
	public void setApplicationVersion(String applicationVersion) {
		this.applicationVersion = applicationVersion;
	}
	public String getDatasourceDriverClassName() {
		return datasourceDriverClassName;
	}
	public void setDatasourceDriverClassName(String datasourceDriverClassName) {
		this.datasourceDriverClassName = datasourceDriverClassName;
	}
	public String getDatasourceUrl() {
		return datasourceUrl;
	}
	public void setDatasourceUrl(String datasourceUrl) {
		this.datasourceUrl = datasourceUrl;
	}
	public String getDatasourceUsername() {
		return datasourceUsername;
	}
	public void setDatasourceUsername(String datasourceUsername) {
		this.datasourceUsername = datasourceUsername;
	}
	public String getDatasourcePassword() {
		return datasourcePassword;
	}
	public void setDatasourcePassword(String datasourcePassword) {
		this.datasourcePassword = datasourcePassword;
	}
	
	
}
