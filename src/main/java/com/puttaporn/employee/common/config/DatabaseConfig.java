package com.puttaporn.employee.common.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

@Configuration
public class DatabaseConfig {
	
	@Autowired
	private ApplicationConfig applicationConfig;
	
	@Bean("dataSource")
	 public DataSource dataSource() {
		DriverManagerDataSource bs = new DriverManagerDataSource();
		 bs.setDriverClassName(applicationConfig.getDatasourceDriverClassName());
		 bs.setUrl(applicationConfig.getDatasourceUrl());
		 bs.setUsername(applicationConfig.getDatasourceUsername());
		 bs.setPassword(applicationConfig.getDatasourcePassword());
		 return bs;
    }
	
	@Bean("namedParameterJdbcTemplate")
	public NamedParameterJdbcTemplate namedParameterJdbcTemplate() {
		return new NamedParameterJdbcTemplate(dataSource());
	}
	
	@Bean("dataSourceTransactionManager")
	public DataSourceTransactionManager dataSourceTransactionManager() {
		return new DataSourceTransactionManager(dataSource());
	}
	

}
