package com.puttaporn.employee.common.model;

public abstract class AbsResponseModel {

	private String statusCode;
	private String statusDesc;
	
	public AbsResponseModel() {
		this.statusCode = "0";
		this.statusDesc = "Success";
	}

	public AbsResponseModel(String statusCode, String statusDesc) {
		super();
		this.statusCode = statusCode;
		this.statusDesc = statusDesc;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatusDesc() {
		return statusDesc;
	}

	public void setStatusDesc(String statusDesc) {
		this.statusDesc = statusDesc;
	}

}
