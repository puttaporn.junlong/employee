package com.puttaporn.employee.common.model;

public class CommonResponseModel extends AbsResponseModel{

	public CommonResponseModel() {
		super();
	}
	
	public CommonResponseModel(String statusCode, String statusDesc) {
		super(statusCode, statusDesc);
	}
}
