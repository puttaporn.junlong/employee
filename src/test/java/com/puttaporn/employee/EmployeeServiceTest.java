package com.puttaporn.employee;

import java.math.BigDecimal;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import com.puttaporn.employee.db.dao.EmployeeDao;
import com.puttaporn.employee.exceptions.BusinessException;
import com.puttaporn.employee.model.request.RegisterEmployeeRequest;
import com.puttaporn.employee.service.impl.EmployeeServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class EmployeeServiceTest {
	
	@Mock
	EmployeeDao employeeDao;
	
	@Spy
	@InjectMocks
	EmployeeServiceImpl employeeServiceImpl;
	
	@Test(expected = BusinessException.class)
	public void testRegisterLessThan15000() throws Exception{
		
		RegisterEmployeeRequest empReq = new RegisterEmployeeRequest();
		empReq.setSalary(new BigDecimal(12000));
		empReq.setPhone("0523614452");
		
		employeeServiceImpl.registerEmployee(empReq);
		
	}
	
	@Test(expected = BusinessException.class)
	public void testRegisterNullSalary() throws Exception{
		
		RegisterEmployeeRequest empReq = new RegisterEmployeeRequest();
		empReq.setSalary(null);
		empReq.setPhone("0523614452");
		
		employeeServiceImpl.registerEmployee(empReq);
		
	}
	
	@Test(expected = BusinessException.class)
	public void testRegisterInvalidPhone() throws Exception{
		RegisterEmployeeRequest empReq = new RegisterEmployeeRequest();
		empReq.setSalary(null);
		empReq.setPhone("saas0523614452");
		
		employeeServiceImpl.registerEmployee(empReq);
	}
	
	@Test(expected = BusinessException.class)
	public void testRegisterInvalidUsername() throws Exception{
		RegisterEmployeeRequest empReq = new RegisterEmployeeRequest();
		empReq.setUsername(null);
		
		employeeServiceImpl.registerEmployee(empReq);
	}
	
	@Test(expected = BusinessException.class)
	public void testRegisterInvalidPassword() throws Exception{
		RegisterEmployeeRequest empReq = new RegisterEmployeeRequest();
		empReq.setUsername("PUTTAPORN");
		empReq.setPassword(null);
		
		employeeServiceImpl.registerEmployee(empReq);
	}
	
}
